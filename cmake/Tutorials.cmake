function(tutorials_copy_and_install extension file_permissions)

  get_filename_component(TUTORIALS_MOD_DIR ${CMAKE_CURRENT_SOURCE_DIR} NAME)

  file(GLOB files ${CMAKE_CURRENT_SOURCE_DIR}/${extension})
  foreach(file ${files})
    file(
      COPY ${file}
      DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/${TUTORIALS_MOD_DIR}
      FILE_PERMISSIONS ${file_permissions})     
    install(FILES ${file}
      DESTINATION ${CMAKE_INSTALL_PREFIX}/${TUTORIALS_MOD_DIR}
      PERMISSIONS ${file_permissions})
  endforeach(file)
endfunction()

function(tutorials_add_executable target source)
  if(MoFEM_PRECOMPILED_HEADRES)
    set_source_files_properties(${source} 
    PROPERTIES COMPILE_FLAGS "-include ${PERCOMPILED_HEADER}")
  endif(MoFEM_PRECOMPILED_HEADRES)
  add_executable(${target} ${source})
endfunction(tutorials_add_executable)

function(tutorials_install tutorials_mod_dir)

  get_filename_component(TUTORIALS_MOD_DIR ${CMAKE_CURRENT_SOURCE_DIR} NAME)

  install(
    DIRECTORY
    ${CMAKE_CURRENT_SOURCE_DIR}
    DESTINATION
    ${CMAKE_INSTALL_PREFIX}/${TUTORIALS_MOD_DIR}
    FILES_MATCHING
    PATTERN "README"
    PATTERN "*.h5m"
    PATTERN "*.cub"
    PATTERN "*.jou"
    PATTERN "*.msh"
    PATTERN "*.cfg"
    PATTERN "*.geo"
    PATTERN "src" EXCLUDE
    PATTERN "doc" EXCLUDE
    PATTERN "atom_tests" EXCLUDE)
endfunction(tutorials_install)

function(tutorials_build_and_install target source)
  tutorials_add_executable(${target} ${source})

  get_filename_component(TUTORIALS_MOD_DIR ${CMAKE_CURRENT_SOURCE_DIR} NAME)

  target_link_libraries(${target}
    # users_modules
    mofem_finite_elements
    mofem_interfaces
    mofem_multi_indices
    mofem_petsc
    mofem_approx
    mofem_third_party
    mofem_matrix_function
    mofem_post_proc
    mofem_boundary_conditions
    ${MoFEM_PROJECT_LIBS})
  install(TARGETS ${target} 
    DESTINATION ${CMAKE_INSTALL_PREFIX}/${TUTORIALS_MOD_DIR})

endfunction(tutorials_build_and_install)

set(MPI_RUN ${MoFEM_MPI_RUN})
