# How to Copy a Tutorial from Core

If you want to modify or experiment with a tutorial, you can do so by copying it to the `users_modules` source directory. This will make the tutorial an independent user module, allowing you to work with it freely. Follow these steps:

1. **Copy the tutorial**: Use the following command to copy the tutorial to your `users_modules` directory:

```bash
cp -r ~/mofem_install/mofem-cephas/mofem/tutorials/vec-0 ~/mofem_install/mofem-cephas/mofem/users_modules/my_vec_0
```

2. **Copy file**: which will indicate that copied tutorial is a module:

```bash
cp ~/mofem_install/mofem-cephas/mofem/users_modules/cmake/InstalledAddModule.tmp  ~/mofem_install/mofem-cephas/mofem/users_modules/my_vec_0/InstalledAddModule.cmake
```

3. **Rebuild the CMake configuration**: Change to the user modules build directory:

Then, rebuild the cache by running:

```bash
make rebuild_cache
```

4. **Compile the code**: Simply run make to compile the code:

```bash
make
```	

Then, test the code by running:
```bash
ctest -VV
```	

Now, you can experiment with the code in your copied tutorial.

5. **Enjoy!** Start exploring and making modifications to your independent user module.
